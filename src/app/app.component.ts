import { Component, OnInit } from '@angular/core';
import { UserService } from "./services/user.service";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
	title = 'toDoApp para SD';
	correo: string = 'test';
	password: string = 'test';
	nombre: string = 'test';
	displayName = '';
	constructor(public userService: UserService) {}
	
	ngOnInit() {
		this.login();
	}
	login() {
		const user = { correo: this.correo, password: this.password};
		this.userService.signIn(user).subscribe(
			(loggedUser) => {
				console.log(loggedUser);
				this.userService.setToken(loggedUser.token);
				console.log("Login correcto");
				console.log(loggedUser.token);
			},
			(err) => {
				console.error("Error en el login");
				console.log(err);
				console.log(user);
			},
			
		);
	}
	
}
